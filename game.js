var canvas, stage;

var offset, dragable;
var COLUM = document.getElementById("colum").value;
var ROW = document.getElementById("row").value;;
var rightPuzzles = 0;
var loader;
var puzzleArray = [];
var pattern = Trianglify({
    width: 800,
    height: 600
});
var image = pattern.canvas();

function init() {

    canvas = document.getElementById("puzzleCanvas");
    stage = new createjs.Stage(canvas);
    createjs.Touch.enable(stage);
    stage.enableMouseOver(10);

    handleImageLoad()
}



function stop() {
    createjs.Ticker.removeEventListener("tick", tick);
}

function handleImageLoad() {

    var container = new createjs.Container();
    var gridContainer = new createjs.Container();

    stage.addChild(gridContainer);
    stage.addChild(container);

    cropPuzzles(image, COLUM, ROW);


    function cropPuzzles(image) {

        var src = image;
        if (!(src.width % 2 || src.heigth % 2)) //Проверка на валидный размер
        {
            var puzzleWidth = src.width / COLUM;
            var puzzleHeight = src.height / ROW;
            DrawPuzzles(puzzleWidth, puzzleHeight)
        } else {
            var error = new createjs.Text("Неправильный размер изображения \nширина и высота должны быть кратны двум.", "18px Arial", "#000000");
            container.addChild(error);
            stop();
        }
    }

    function DrawPuzzles(puzzleWidth, puzzleHeight) {
        for (var r = 0; r < ROW; r++) {
            for (var c = 0; c < COLUM; c++) {
                var puzzle = new createjs.Shape();
                var grid = new createjs.Shape();
                var localX = c * puzzleWidth;
                var localY = r * puzzleHeight;


                var down = rnd(1, 2);
                var right = rnd(1, 2);



                //EDGES
                if (r == 0) {
                    up = 0;
                }
                if (r == ROW - 1) {
                    down = 0;
                }
                if (c == 0) {
                    left = 0;
                }
                if (c == COLUM - 1) {
                    right = 0;
                }

                //UP
                if (r > 0 && r < ROW) {

                    if (puzzleArray[puzzleArray.length - COLUM].down == 2) {
                        up = 1;
                    } else {
                        up = 2;
                    }
                }
                //LEFT
                if (c > 0 && c < COLUM) {
                    if (puzzleArray[puzzleArray.length - 1].right == 2) {
                        left = 1;
                    } else {
                        left = 2;
                    }
                }

                function rnd(min, max) {
                    return Math.floor(Math.random() * (max - min + 1)) + min
                }


                puzzle.left = left;
                puzzle.down = down;
                puzzle.right = right;
                puzzle.up = up;


                CreatePuzzle(left, down, right, up);

                function CreatePuzzle(left, down, right, up) {
                    puzzle.graphics.mt(localX, localY)

                    switch (left) {

                        case 0:
                            puzzle
                                .graphics.lt(localX, localY + puzzleHeight);
                            break;
                        case 1:
                            puzzle.graphics
                                .lt(localX, localY + puzzleHeight / 3.78)
                                .bt(localX, localY + puzzleHeight / 2.67, localX, localY + puzzleHeight / 2.44, localX + puzzleWidth / 22.12, localY + puzzleHeight / 2.64)
                                .bt(localX + puzzleWidth / 13.54, localY + puzzleHeight / 2.77, localX + puzzleWidth / 9.04, localY + puzzleHeight / 2.84, localX + puzzleWidth / 6.90, localY + puzzleHeight / 2.84)
                                .bt(localX + puzzleWidth / 4.38, localY + puzzleHeight / 2.84, localX + puzzleWidth / 3.38, localY + puzzleHeight / 2.39, localX + puzzleWidth / 3.38, localY + puzzleHeight / 1.99)
                                .bt(localX + puzzleWidth / 3.38, localY + puzzleHeight / 1.71, localX + puzzleWidth / 4.38, localY + puzzleHeight / 1.53, localX + puzzleWidth / 6.90, localY + puzzleHeight / 1.53)
                                .bt(localX + puzzleWidth / 9.04, localY + puzzleHeight / 1.53, localX + puzzleWidth / 13.54, localY + puzzleHeight / 1.56, localX + puzzleWidth / 22.12, localY + puzzleHeight / 1.60)
                                .bt(localX, localY + puzzleHeight / 1.68, localX, localY + puzzleHeight / 1.59, localX, localY + puzzleHeight / 1.35)
                                .lt(localX, localY + puzzleHeight)
                            break;
                        case 2:
                            puzzle.graphics
                                .lt(localX, localY + puzzleHeight / 3.78)
                                .bt(localX, localY + puzzleHeight / 2.64, localX, localY + puzzleHeight / 2.42, localX + puzzleWidth / -17.63, localY + puzzleHeight / 2.61)
                                .bt(localX + puzzleWidth / -11.74, localY + puzzleHeight / 2.74, localX + puzzleWidth / -8.235, localY + puzzleHeight / 2.81, localX + puzzleWidth / -6.43, localY + puzzleHeight / 2.81)
                                .bt(localX + puzzleWidth / -4.20, localY + puzzleHeight / 2.81, localX + puzzleWidth / -3.28, localY + puzzleHeight / 2.36, localX + puzzleWidth / -3.28, localY + puzzleHeight / 1.97)
                                .bt(localX + puzzleWidth / -3.28, localY + puzzleHeight / 1.69, localX + puzzleWidth / -4.20, localY + puzzleHeight / 1.52, localX + puzzleWidth / -6.43, localY + puzzleHeight / 1.52)
                                .bt(localX + puzzleWidth / -8.235, localY + puzzleHeight / 1.52, localX + puzzleWidth / -11.74, localY + puzzleHeight / 1.54, localX + puzzleWidth / -17.63, localY + puzzleHeight / 1.58)
                                .bt(localX, localY + puzzleHeight / 1.67, localX, localY + puzzleHeight / 1.57, localX, localY + puzzleHeight / 1.34)
                                .lt(localX, localY + puzzleHeight)
                            break
                    }
                    switch (down) {
                        case 0:
                            puzzle.graphics
                                .lt(localX + puzzleWidth, localY + puzzleHeight)
                            break;
                        case 1:
                            puzzle.graphics
                                .lt(localX + puzzleWidth / 3.85, localY + puzzleHeight)
                                .bt(localX + puzzleWidth / 2.70, localY + puzzleHeight, localX + puzzleWidth / 2.47, localY + puzzleHeight, localX + puzzleWidth / 2.67, localY + puzzleHeight / 1.05)
                                .bt(localX + puzzleWidth / 2.81, localY + puzzleHeight / 1.08, localX + puzzleWidth / 2.88, localY + puzzleHeight / 1.12, localX + puzzleWidth / 2.88, localY + puzzleHeight / 1.17)
                                .bt(localX + puzzleWidth / 2.88, localY + puzzleHeight / 1.36, localX + puzzleWidth / 2.41, localY + puzzleHeight / 1.40, localX + puzzleWidth / 2.01, localY + puzzleHeight / 1.42)
                                .bt(localX + puzzleWidth / 1.72, localY + puzzleHeight / 1.40, localX + puzzleWidth / 1.54, localY + puzzleHeight / 1.36, localX + puzzleWidth / 1.54, localY + puzzleHeight / 1.17)
                                .bt(localX + puzzleWidth / 1.54, localY + puzzleHeight / 1.12, localX + puzzleWidth / 1.57, localY + puzzleHeight / 1.08, localX + puzzleWidth / 1.61, localY + puzzleHeight / 1.05)
                                .bt(localX + puzzleWidth / 1.70, localY + puzzleHeight, localX + puzzleWidth / 1.60, localY + puzzleHeight, localX + puzzleWidth / 1.36, localY + puzzleHeight)
                                .lt(localX + puzzleWidth, localY + puzzleHeight)
                            break;
                        case 2:
                            puzzle.graphics
                                .bt(localX + puzzleWidth / 2.70, localY + puzzleHeight, localX + puzzleWidth / 2.47, localY + puzzleHeight, localX + puzzleWidth / 2.67, localY + puzzleHeight / 0.94)
                                .bt(localX + puzzleWidth / 2.81, localY + puzzleHeight / 0.92, localX + puzzleWidth / 2.89, localY + puzzleHeight / 0.89, localX + puzzleWidth / 2.89, localY + puzzleHeight / 0.86)
                                .bt(localX + puzzleWidth / 2.89, localY + puzzleHeight / 0.80, localX + puzzleWidth / 2.41, localY + puzzleHeight / 0.76, localX + puzzleWidth / 2.01, localY + puzzleHeight / 0.76)
                                .bt(localX + puzzleWidth / 1.72, localY + puzzleHeight / 0.76, localX + puzzleWidth / 1.54, localY + puzzleHeight / 0.80, localX + puzzleWidth / 1.54, localY + puzzleHeight / 0.86)
                                .bt(localX + puzzleWidth / 1.54, localY + puzzleHeight / 0.89, localX + puzzleWidth / 1.56, localY + puzzleHeight / 0.92, localX + puzzleWidth / 1.61, localY + puzzleHeight / 0.94)
                                .bt(localX + puzzleWidth / 1.69, localY + puzzleHeight, localX + puzzleWidth / 1.60, localY + puzzleHeight, localX + puzzleWidth / 1.35, localY + puzzleHeight)
                                .lt(localX + puzzleWidth, localY + puzzleHeight)
                            break;
                    }
                    switch (right) {
                        case 0:
                            puzzle.graphics
                                .lt(localX + puzzleWidth, localY)
                            break;
                        case 1:
                            puzzle.graphics
                                .lt(localX + puzzleWidth, localY + puzzleHeight / 1.35)
                                .bt(localX + puzzleWidth, localY + puzzleHeight / 1.59, localX + puzzleWidth / 1.008, localY + puzzleHeight / 1.68, localX + puzzleWidth / 1.06, localY + puzzleHeight / 1.60)
                                .bt(localX + puzzleWidth / 1.09, localY + puzzleHeight / 1.56, localX + puzzleWidth / 1.13, localY + puzzleHeight / 1.53, localX + puzzleWidth / 1.18, localY + puzzleHeight / 1.53)
                                .bt(localX + puzzleWidth / 1.31, localY + puzzleHeight / 1.53, localX + puzzleWidth / 1.44, localY + puzzleHeight / 1.71, localX + puzzleWidth / 1.44, localY + puzzleHeight / 1.99)
                                .bt(localX + puzzleWidth / 1.44, localY + puzzleHeight / 2.39, localX + puzzleWidth / 1.31, localY + puzzleHeight / 2.84, localX + puzzleWidth / 1.18, localY + puzzleHeight / 2.84)
                                .bt(localX + puzzleWidth / 1.13, localY + puzzleHeight / 2.84, localX + puzzleWidth / 1.09, localY + puzzleHeight / 2.77, localX + puzzleWidth / 1.06, localY + puzzleHeight / 2.64)
                                .bt(localX + puzzleWidth / 1.008, localY + puzzleHeight / 2.44, localX + puzzleWidth, localY + puzzleHeight / 2.67, localX + puzzleWidth, localY + puzzleHeight / 3.78)
                                .lt(localX + puzzleWidth, localY + puzzleHeight / 4.04)
                                .lt(localX + puzzleWidth, localY)
                            break;
                        case 2:
                            puzzle.graphics
                                .lt(localX + puzzleWidth, localY + puzzleHeight / 1.35)
                                .bt(localX + puzzleWidth, localY + puzzleHeight / 1.57, localX + puzzleWidth, localY + puzzleHeight / 1.67, localX + puzzleWidth / 0.95, localY + puzzleHeight / 1.58)
                                .bt(localX + puzzleWidth / 0.93, localY + puzzleHeight / 1.54, localX + puzzleWidth / 0.90, localY + puzzleHeight / 1.52, localX + puzzleWidth / 0.87, localY + puzzleHeight / 1.52)
                                .bt(localX + puzzleWidth / 0.81, localY + puzzleHeight / 1.52, localX + puzzleWidth / 0.77, localY + puzzleHeight / 1.69, localX + puzzleWidth / 0.77, localY + puzzleHeight / 1.97)
                                .bt(localX + puzzleWidth / 0.77, localY + puzzleHeight / 2.36, localX + puzzleWidth / 0.81, localY + puzzleHeight / 2.81, localX + puzzleWidth / 0.87, localY + puzzleHeight / 2.81)
                                .bt(localX + puzzleWidth / 0.90, localY + puzzleHeight / 2.81, localX + puzzleWidth / 0.93, localY + puzzleHeight / 2.74, localX + puzzleWidth / 0.95, localY + puzzleHeight / 2.61)
                                .bt(localX + puzzleWidth, localY + puzzleHeight / 2.42, localX + puzzleWidth, localY + puzzleHeight / 2.64, localX + puzzleWidth, localY + puzzleHeight / 3.74)
                                .lt(localX + puzzleWidth, localY + puzzleHeight / 4.04)
                                .lt(localX + puzzleWidth, localY)
                            break;
                    }
                    switch (up) {
                        case 0:
                            puzzle.graphics
                                .lt(localX, localY)
                            break;

                        case 1:
                            puzzle.graphics
                                .lt(localX + puzzleWidth / 1.36, localY)
                                .bt(localX + puzzleWidth / 1.60, localY, localX + puzzleWidth / 1.70, localY, localX + puzzleWidth / 1.61, localY + puzzleHeight / 18.51)
                                .bt(localX + puzzleWidth / 1.57, localY + puzzleHeight / 12.10, localX + puzzleWidth / 1.54, localY + puzzleHeight / 8.37, localX + puzzleWidth / 1.54, localY + puzzleHeight / 6.50)
                                .bt(localX + puzzleWidth / 1.54, localY + puzzleHeight / 4.22, localX + puzzleWidth / 1.72, localY + puzzleHeight / 3.28, localX + puzzleWidth / 2.01, localY + puzzleHeight / 3.28)
                                .bt(localX + puzzleWidth / 2.41, localY + puzzleHeight / 3.28, localX + puzzleWidth / 2.88, localY + puzzleHeight / 4.22, localX + puzzleWidth / 2.88, localY + puzzleHeight / 6.50)
                                .bt(localX + puzzleWidth / 2.88, localY + puzzleHeight / 8.37, localX + puzzleWidth / 2.81, localY + puzzleHeight / 12.10, localX + puzzleWidth / 2.67, localY + puzzleHeight / 18.51)
                                .bt(localX + puzzleWidth / 2.47, localY, localX + puzzleWidth / 2.70, localY, localX + puzzleWidth / 3.85, localY)
                                .lt(localX, localY)
                            break;
                        case 2:
                            puzzle.graphics

                                .lt(localX + puzzleWidth / 1.36, localY)
                                .bt(localX + puzzleWidth / 1.60, localY, localX + puzzleWidth / 1.69, localY, localX + puzzleWidth / 1.61, localY + puzzleHeight / -20.90)
                                .bt(localX + puzzleWidth / 1.56, localY + puzzleHeight / -13.13, localX + puzzleWidth / 1.54, localY + puzzleHeight / -8.88, localX + puzzleWidth / 1.54, localY + puzzleHeight / -6.82)
                                .bt(localX + puzzleWidth / 1.54, localY + puzzleHeight / -4.36, localX + puzzleWidth / 1.72, localY + puzzleHeight / -3.38, localX + puzzleWidth / 2.01, localY + puzzleHeight / -3.38)
                                .bt(localX + puzzleWidth / 2.41, localY + puzzleHeight / -3.38, localX + puzzleWidth / 2.89, localY + puzzleHeight / -4.36, localX + puzzleWidth / 2.89, localY + puzzleHeight / -6.82)
                                .bt(localX + puzzleWidth / 2.89, localY + puzzleHeight / -8.88, localX + puzzleWidth / 2.81, localY + puzzleHeight / -13.13, localX + puzzleWidth / 2.67, localY + puzzleHeight / -20.90)
                                .bt(localX + puzzleWidth / 2.47, localY, localX + puzzleWidth / 2.70, localY, localX + puzzleWidth / 3.87, localY)
                                .lt(localX + puzzleWidth / 4.18, localY)
                                .lt(localX, localY)
                            break;
                    }
                }

                grid = puzzle.clone(true);
                puzzle.graphics.append(new createjs.Graphics.Fill().bitmap(image));
                puzzle.graphics.append(new createjs.Graphics.Stroke("rgba(0,0,0,0.3)"));
                // puzzle.graphics.beginFill("#8f0c10").drawRect(0, 0, 20, 20); //Реальный центр

                puzzleArray.push(puzzle);
                container.addChild(puzzle);

                grid.graphics.append(new createjs.Graphics.Fill());
                grid.graphics.append(new createjs.Graphics.Stroke("rgba(0,0,0,0.1)"));
                gridContainer.addChild(grid);
                puzzle.x = (-localX) + (Math.random() * ((canvas.width - (puzzleWidth * 2) / 2) - puzzleWidth / 2) + puzzleWidth / 2) | 0;
                puzzle.y = (-localY) + (Math.random() * ((canvas.height - (puzzleHeight * 2) / 2) - puzzleHeight / 2) + puzzleHeight / 2) | 0;

                puzzle.scaleX = puzzle.scaleY = puzzle.scale = 1;
                puzzle.name = "puzzle[" + c + "][" + r + "]";
                puzzle.cursor = "pointer";
                puzzle.dragable = true;

                puzzle.on("mousedown", function (evt) {
                    if (this.dragable) {
                        this.parent.addChild(this);
                        this.offset = {x: this.x - evt.stageX, y: this.y - evt.stageY};
                    }
                });

                puzzle.on("pressup", function (evt) {

                    if (((this.x > -20 && this.x <= 20) && (this.y > -20 && this.y <= 20)) && this.dragable) {

                        this.cursor = "default";
                        this.dragable = false;
                        rightPuzzles++;
                        createjs.Tween.get(this).to({
                            x: 0,
                            y: 0
                        }, 300, createjs.Ease.getPowInOut(2)).call(function () {
                            container.setChildIndex(this, 0);
                        })
                    }

                    if (rightPuzzles == (COLUM * ROW)) {
                        var fade = new createjs.Shape();
                        fade.graphics.beginFill('#000000').drawRect(0, 0, canvas.width, canvas.height);
                        fade.alpha = 0;
                        container.addChild(fade);
                        createjs.Tween.get(fade).to({alpha: 0.5}, 500).call(function () {
                            stop();
                        })

                    }
                })

                // the pressmove event is dispatched when the mouse moves after a mousedown on the target until the mouse is released.
                puzzle.on("pressmove", function (evt) {
                    if (this.dragable) {
                        this.x = evt.stageX + this.offset.x;
                        this.y = evt.stageY + this.offset.y;
                    }
                });


            }
        }

    }


    createjs.Ticker.framerate = 30;
    createjs.Ticker.addEventListener("tick", tick);
}

function hidePreview(){
    var container = document.getElementById("Preview");

    if (container.contains(image)) {
        container.removeChild(image);
    }  else {
        container.appendChild(image);
    }
}
function tick(event) {
    stage.update(event);
}